package com.mhr.simpletakeaway.data.repository

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.mhr.simpletakeaway.data.datasource.RestaurantCacheDataSource
import com.mhr.simpletakeaway.data.datasource.RestaurantFileDataSource
import com.mhr.simpletakeaway.datasource.cache.db.AppDatabase
import com.mhr.simpletakeaway.domain.model.Restaurant
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.io.IOException

class RestaurantRepositoryImplTest {

    private lateinit var db: AppDatabase
    private lateinit var dao: RestaurantCacheDataSource
    private val compositeDisposable = CompositeDisposable()
    @Mock
    private lateinit var fileDataSource: RestaurantFileDataSource

    private lateinit var restaurantRepository: RestaurantRepositoryImpl

    @Before
    @Throws(IOException::class)
    fun createDb() {

        MockitoAnnotations.initMocks(this)

        RxJavaPlugins.setIoSchedulerHandler {
            Schedulers.trampoline()
        }

        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context,
            AppDatabase::class.java
        ).build()
        dao = db.restaurantDao()

        `when`(fileDataSource.getAll()).thenReturn(
            Single.just(
                listOf(
                    Restaurant("1"),
                    Restaurant("2"),
                    Restaurant("3"),
                    Restaurant("4")
                )
            )
        )

        restaurantRepository = RestaurantRepositoryImpl(dao, fileDataSource)

    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
        compositeDisposable.dispose()
    }

    @Test
    fun getAll() {

        val getFirstFromDbObserver = dao.getAll().test()
        compositeDisposable.add(getFirstFromDbObserver)

        getFirstFromDbObserver.assertNoErrors()
        getFirstFromDbObserver.assertComplete()
        getFirstFromDbObserver.assertValue {
            it.isEmpty()
        }

        val getAllFromRepoObserver = restaurantRepository.getAll().test()
        compositeDisposable.add(getAllFromRepoObserver)
        var restaurants: List<Restaurant> = listOf()

        getAllFromRepoObserver.assertNoErrors()
        getAllFromRepoObserver.assertComplete()
        getAllFromRepoObserver.assertValue {
            restaurants = it.first
            it.first.size == 4 && it.second.not()
        }

        val setAllObserver = restaurantRepository.setAll(restaurants).test()
        compositeDisposable.add(setAllObserver)

        val nowGetFromDbAgainObserver = dao.getAll().test()
        compositeDisposable.add(nowGetFromDbAgainObserver)

        nowGetFromDbAgainObserver.assertNoErrors()
        nowGetFromDbAgainObserver.assertComplete()
        nowGetFromDbAgainObserver.assertValue {
            it.size == 4
        }

    }
}