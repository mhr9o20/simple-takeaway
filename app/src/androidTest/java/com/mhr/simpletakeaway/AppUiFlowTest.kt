package com.mhr.simpletakeaway

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.mhr.simpletakeaway.presentation.MainActivity
import com.mhr.simpletakeaway.presentation.restaurants.RestaurantsFragment
import com.mhr.simpletakeaway.presentation.restaurants.sortfilters.SortFilterViewHolder
import junit.framework.Assert.assertNotNull
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class AppUiFlowTest {

    @get:Rule
    val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testFlow() {

        val initialFragment = activityRule.activity.supportFragmentManager.fragments[0]
        assertNotNull(initialFragment)
        assert(initialFragment is RestaurantsFragment)

        val expectedTitle = activityRule.activity.getString(R.string.restaurants_label)
        val title = activityRule.activity.supportActionBar?.title?.toString()
        assertNotNull(title)
        assert(title == expectedTitle)

        onView(withId(R.id.restaurantsFilterFab)).check(matches(isDisplayed()))

        onView(withId(R.id.restaurantsFilterFab)).perform(ViewActions.click())
        Thread.sleep(1000)

        onView(withId(R.id.sortFilterModalRecyclerView)).check(matches(isDisplayed()))
        onView(withId(R.id.sortFilterModalRecyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<SortFilterViewHolder>(0, click())
        )
        Thread.sleep(1000)

        onView(withId(R.id.sortFilterModalRecyclerView)).check(doesNotExist())

        onView(withId(R.id.restaurants_search)).perform(click())
        Thread.sleep(1000)

        val expectedSearchTitle = activityRule.activity.getString(R.string.search_label)
        val currentTitle = activityRule.activity.supportActionBar?.title?.toString()
        assertNotNull(currentTitle)
        assert(currentTitle == expectedSearchTitle)

        onView(withId(R.id.searchEmptyPlaceHolderImageView)).check(matches(isDisplayed()))
        onView(withId(R.id.searchEmptyPlaceHolderImageView)).check(matches(isDisplayed()))
        onView(withId(R.id.searchEmptyPlaceHolderTextView)).check(matches(withText("")))

        onView(withId(R.id.searchInputEditText)).perform(typeText("aa"))

        Thread.sleep(1000)

        onView(withId(R.id.searchEmptyPlaceHolderImageView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.searchEmptyPlaceHolderImageView)).check(matches(not(isDisplayed())))

        onView(withId(R.id.searchInputEditText)).perform(typeText("aaaa"))

        Thread.sleep(1000)

        onView(withId(R.id.searchEmptyPlaceHolderImageView)).check(matches(isDisplayed()))
        onView(withId(R.id.searchEmptyPlaceHolderImageView)).check(matches(isDisplayed()))
        onView(withId(R.id.searchEmptyPlaceHolderTextView)).check(matches(not(withText(""))))

        onView(withId(R.id.searchCloseButton)).perform(click())

        Thread.sleep(1000)

        val finalExpectedTitle = activityRule.activity.getString(R.string.restaurants_label)
        val finalTitle = activityRule.activity.supportActionBar?.title?.toString()
        assertNotNull(title)
        assert(finalTitle == finalExpectedTitle)

    }

}