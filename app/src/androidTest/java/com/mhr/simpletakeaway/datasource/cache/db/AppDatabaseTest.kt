package com.mhr.simpletakeaway.datasource.cache.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mhr.simpletakeaway.data.datasource.RestaurantCacheDataSource
import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.model.SortingValues
import com.mhr.simpletakeaway.domain.model.Status
import io.reactivex.disposables.CompositeDisposable
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest {

    private lateinit var db: AppDatabase
    private lateinit var dao: RestaurantCacheDataSource
    private val compositeDisposable = CompositeDisposable()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context,
            AppDatabase::class.java
        ).build()
        dao = db.restaurantDao()
    }

    @Test
    @Throws(IOException::class)
    fun testWritesAndReads() {

        val restaurant1 = Restaurant(
            name = "Restaurant 1",
            status = Status.Open,
            isFavorite = false,
            sortingValues = SortingValues(
                bestMatch = 10.0,
                newest = 20.0,
                ratingAverage = 4.5,
                distance = 1500.0,
                popularity = 200.0,
                averageProductPrice = 450.0,
                deliveryCosts = 200.0,
                minCost = 120.5
            )
        )

        val restaurant2 = Restaurant(
            name = "Restaurant 2",
            status = Status.OrderAhead,
            isFavorite = false,
            sortingValues = SortingValues(
                bestMatch = 20.0,
                newest = 10.0,
                ratingAverage = 4.0,
                distance = 1670.0,
                popularity = 100.0,
                averageProductPrice = 750.0,
                deliveryCosts = 0.0,
                minCost = 500.0
            )
        )

        val restaurants = listOf(restaurant1, restaurant2)

        val setAllObserver = dao.setAll(restaurants).test()
        compositeDisposable.add(setAllObserver)

        setAllObserver.assertComplete()
        setAllObserver.assertNoErrors()

        val getAllObserver = dao.getAll().test()
        compositeDisposable.addAll(getAllObserver)

        getAllObserver.assertComplete()
        getAllObserver.assertNoErrors()
        getAllObserver.assertValue {
            it.isNotEmpty() &&
                    it.size == 2 &&
                    it.contains(restaurant1) &&
                    it.contains(restaurant2)

        }

        restaurant1.isFavorite = true
        val setObserver = dao.set(restaurant1).test()
        val getAllAfterSetObserver = dao.getAll().test()
        val getAfterSetObserver = dao.get(restaurant1.name).test()
        compositeDisposable.addAll(setObserver, getAllAfterSetObserver, getAfterSetObserver)

        setObserver.assertNoErrors()
        setObserver.assertComplete()

        getAllAfterSetObserver.assertNoErrors()
        getAllAfterSetObserver.assertComplete()
        getAllAfterSetObserver.assertValue {
            it.size == 2
        }

        getAfterSetObserver.assertNoErrors()
        getAfterSetObserver.assertComplete()
        getAfterSetObserver.assertValue {
            it.isFavorite
        }


        restaurant2.isFavorite = true
        val updateObserver = dao.update(restaurant2).test()
        val getAllAfterUpdateObserver = dao.getAll().test()
        val getAfterUpdateObserver = dao.get(restaurant2.name).test()
        compositeDisposable.addAll(updateObserver, getAllAfterUpdateObserver, getAfterUpdateObserver)

        updateObserver.assertNoErrors()
        updateObserver.assertComplete()

        getAllAfterUpdateObserver.assertNoErrors()
        getAllAfterUpdateObserver.assertComplete()
        getAllAfterUpdateObserver.assertValue {
            it.size == 2
        }

        getAfterUpdateObserver.assertNoErrors()
        getAfterUpdateObserver.assertComplete()
        getAfterUpdateObserver.assertValue {
            it.isFavorite
        }



    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
        compositeDisposable.dispose()
    }
}