package com.mhr.simpletakeaway

import androidx.multidex.MultiDexApplication
import com.mhr.simpletakeaway.di.AppComponent
import com.mhr.simpletakeaway.di.DaggerAppComponent

class App : MultiDexApplication() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private fun initDagger() {
        component = DaggerAppComponent
            .factory()
            .create(this)
    }

}