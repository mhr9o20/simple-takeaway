package com.mhr.simpletakeaway.domain.usecase

import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.repository.RestaurantRepository
import io.reactivex.Single

class SearchUseCase(private val restaurantRepository: RestaurantRepository) {
    fun queryBy(name: String): Single<List<Restaurant>> {
        return restaurantRepository
            .getAll()
            .flattenAsObservable { it.first }
            .filter {
                val queryWithNoSpace = name.replace("\\s".toRegex(), "")
                val nameWithNoSpace = it.name.replace("\\s".toRegex(), "")
                nameWithNoSpace.contains(queryWithNoSpace, true)
            }
            .toList()
    }
}