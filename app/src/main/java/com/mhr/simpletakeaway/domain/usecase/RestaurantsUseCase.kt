package com.mhr.simpletakeaway.domain.usecase

import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.repository.RestaurantRepository
import io.reactivex.Completable
import io.reactivex.Single

class RestaurantsUseCase(private val restaurantRepository: RestaurantRepository) {

    fun getAll(): Single<Pair<List<Restaurant>, Boolean>> {
        return restaurantRepository.getAll()
    }

    fun getBy(name: String): Single<Restaurant> {
        return restaurantRepository.getBy(name).toSingle()
    }

    fun set(restaurant: Restaurant): Completable {
        return restaurantRepository.set(restaurant)
    }

    fun update(restaurant: Restaurant): Completable {
        return restaurantRepository.update(restaurant)
    }

    fun setAll(restaurants: List<Restaurant>): Completable {
        return restaurantRepository.setAll(restaurants)
    }

}