package com.mhr.simpletakeaway.domain.repository

import com.mhr.simpletakeaway.domain.model.Restaurant
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface RestaurantRepository {

    fun getAll(): Single<Pair<List<Restaurant>, Boolean>>

    fun getBy(name: String): Maybe<Restaurant>

    fun set(restaurant: Restaurant): Completable

    fun update(restaurant: Restaurant): Completable

    fun setAll(restaurants: List<Restaurant>): Completable

}