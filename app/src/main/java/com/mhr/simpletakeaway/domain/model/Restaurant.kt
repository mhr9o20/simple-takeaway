package com.mhr.simpletakeaway.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mhr.simpletakeaway.presentation.model.RestaurantItem

@Entity
data class Restaurant(
    @PrimaryKey val name: String,
    var status: String = Status.Closed,
    val sortingValues: SortingValues = SortingValues(),
    var isFavorite: Boolean = false
) {

    /*
     * Ideally we could have IDs for each, but concerning the data set, as the names are unique,
     * we continue with names as our primary key and equality factor.
     */

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Restaurant

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

data class SortingValues(
    val bestMatch: Double = 0.0,
    val newest: Double = 0.0,
    val ratingAverage: Double = 0.0,
    val distance: Double = 0.0,
    val popularity: Double = 0.0,
    val averageProductPrice: Double = 0.0,
    val deliveryCosts: Double = 0.0,
    val minCost: Double = 0.0
)

class Status {
    companion object {
        const val Open = "open"
        const val Closed = "closed"
        const val OrderAhead = "order ahead"
    }
}

fun Restaurant.mapToPresentationModel(): RestaurantItem {
    val status = when (status) {
        Status.Open -> com.mhr.simpletakeaway.presentation.model.Status.Open
        Status.OrderAhead -> com.mhr.simpletakeaway.presentation.model.Status.OrderAhead
        Status.Closed -> com.mhr.simpletakeaway.presentation.model.Status.Closed
        else -> com.mhr.simpletakeaway.presentation.model.Status.Closed
    }

    return RestaurantItem(
        name = name,
        status = status,
        isFavorite = isFavorite,
        bestMatchScore = sortingValues.bestMatch,
        newestScore = sortingValues.newest,
        ratingAverage = sortingValues.ratingAverage,
        distance = sortingValues.distance,
        popularityScore = sortingValues.popularity,
        averageProductPrice = sortingValues.averageProductPrice,
        deliveryCosts = sortingValues.deliveryCosts,
        minimumCost = sortingValues.minCost
    )

}

fun List<Restaurant>.mapToPresentationModel(): List<RestaurantItem> = map { it.mapToPresentationModel() }

