package com.mhr.simpletakeaway.presentation.restaurants.sortfilters

import android.content.Context
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.presentation.model.SortFilter
import io.reactivex.Observable
import kotlinx.android.synthetic.main.view_sort_filter_modal.*
import java.util.concurrent.TimeUnit

class SortFiltersModal(
    context: Context,
    defaultSortFilter: SortFilter = SortFilter.Rating
) {

    private val dialog: BottomSheetDialog = BottomSheetDialog(context)
    private var adapter: SortFilterAdapter

    private val closeEmissionDelay = 500L

    init {
        dialog.setContentView(R.layout.view_sort_filter_modal)
        dialog.setCanceledOnTouchOutside(true)
        dialog.dismissWithAnimation = true
        (dialog.sortFilterModalRecyclerView.itemAnimator as? SimpleItemAnimator)
            ?.supportsChangeAnimations = false
        dialog.setOnDismissListener {
            dialog.cancel()
        }
        adapter = SortFilterAdapter(
            SortFilter.values().toList(),
            defaultSortFilter
        )
    }

    fun show() {
        dialog.sortFilterModalRecyclerView.adapter = adapter
        dialog.show()
    }

    fun dismiss() {
        adapter.dispose()
        dialog.dismiss()
    }

    fun onFilterSelections(): Observable<SortFilter> =
        adapter.onFilterSelections()
            .delay(
                closeEmissionDelay,
                TimeUnit.MILLISECONDS
            )
            .doOnNext {
                dismiss()
            }


}