package com.mhr.simpletakeaway.presentation.restaurants

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxrelay2.PublishRelay
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.presentation.model.RestaurantItem
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class RestaurantAdapter: RecyclerView.Adapter<RestaurantViewHolder>() {

    private val items: MutableList<RestaurantItem> = mutableListOf()
    private val onFavouriteClicks: PublishRelay<RestaurantItem> = PublishRelay.create()
    private val compositeDisposable = CompositeDisposable()

    fun onFavouriteClicks(): Observable<RestaurantItem> = onFavouriteClicks.hide()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        return RestaurantViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(
                    R.layout.view_restaurant_item,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        if (position >= 0 && position < items.size) {
            holder.bind(items[position])
                ?.subscribe {
                    onFavouriteClicks.accept(it)
                }
                ?.let {
                    compositeDisposable.add(it)
                }
        }
    }

    override fun getItemId(position: Int): Long {
        return if (position >= 0 && position < items.size) {
            items[position].name.hashCode().toLong()
        } else {
            super.getItemId(position)
        }
    }

    fun update(data: List<RestaurantItem>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    fun dispose() {
        compositeDisposable.dispose()
    }

}