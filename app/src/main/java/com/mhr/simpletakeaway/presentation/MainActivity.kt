package com.mhr.simpletakeaway.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.mhr.simpletakeaway.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findNavController(R.id.mainNavHostFragment).addOnDestinationChangedListener {
                _,
                destination,
                _ ->

            supportActionBar?.title = destination.label

        }

    }
}
