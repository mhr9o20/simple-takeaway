package com.mhr.simpletakeaway.presentation.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.model.SortingValues

enum class Status(@DrawableRes val icon: Int, @StringRes val title: Int) {
    Open(
        R.drawable.ic_open_24dp,
        R.string.status_open
    ),
    OrderAhead(
        R.drawable.ic_order_ahead_24dp,
        R.string.status_order_ahead
    ),
    Closed(
        R.drawable.ic_closed_24dp,
        R.string.status_closed
    )
}

data class RestaurantItem(
    val name: String,
    val status: Status,
    var isFavorite: Boolean,
    val bestMatchScore: Double,
    val newestScore: Double,
    val ratingAverage: Double,
    val distance: Double,
    val popularityScore: Double,
    val averageProductPrice: Double,
    val deliveryCosts: Double,
    val minimumCost: Double
) {

    var sortingScore: Double = 0.0
    var filters: ArrayList<SortFilter>? = null

    fun applySortFilters(filters: ArrayList<SortFilter>) {
        this.filters = filters
        sortingScore = 0.0
        for (filter in filters) {
            when (filter) {
                SortFilter.Rating -> sortingScore += ratingAverage
                SortFilter.Popularity -> sortingScore += popularityScore
                SortFilter.Newest -> sortingScore += newestScore
                SortFilter.BestMatch -> sortingScore += bestMatchScore
                SortFilter.MinimumCost -> sortingScore -= minimumCost
                SortFilter.Distance -> sortingScore -= distance
                SortFilter.DeliveryCost -> sortingScore -= deliveryCosts
                SortFilter.AveragePrice -> sortingScore -= averageProductPrice
            }
        }
    }

}

fun RestaurantItem.mapToDomainModel(): Restaurant {
    val status = when (status) {
        Status.Open -> com.mhr.simpletakeaway.domain.model.Status.Open
        Status.OrderAhead -> com.mhr.simpletakeaway.domain.model.Status.OrderAhead
        Status.Closed -> com.mhr.simpletakeaway.domain.model.Status.Closed
    }

    return Restaurant(
        name = name,
        status = status,
        isFavorite = isFavorite,
        sortingValues = SortingValues(
            bestMatch = bestMatchScore,
            ratingAverage = ratingAverage,
            popularity = popularityScore,
            newest = newestScore,
            minCost = minimumCost,
            distance = distance,
            deliveryCosts = deliveryCosts,
            averageProductPrice = averageProductPrice
        )
    )

}

fun List<RestaurantItem>.mapToDomainModel(): List<Restaurant> = map { it.mapToDomainModel() }