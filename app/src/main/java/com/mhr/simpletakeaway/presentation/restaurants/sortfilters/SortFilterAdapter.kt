package com.mhr.simpletakeaway.presentation.restaurants.sortfilters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxrelay2.PublishRelay
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.presentation.model.SortFilter
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class SortFilterAdapter(
    private val items: List<SortFilter>,
    defaultFilter: SortFilter
) : RecyclerView.Adapter<SortFilterViewHolder>() {

    private val compositeDisposable = CompositeDisposable()
    private var selectedFilterPosition = items.indexOf(defaultFilter)
    private val onFilterSelections: PublishRelay<SortFilter> = PublishRelay.create()

    fun onFilterSelections(): Observable<SortFilter> = onFilterSelections.hide()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortFilterViewHolder {
        return SortFilterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.view_sort_filter_item,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SortFilterViewHolder, position: Int) {
        if (position >= 0 && position < items.size) {
            holder.bind(
                items[position],
                position == selectedFilterPosition
            )
                ?.filter {
                    items.indexOf(it) != selectedFilterPosition
                }
                ?.subscribe {

                    val itemToDeselectIndex = selectedFilterPosition
                    selectedFilterPosition = items.indexOf(it)

                    notifyItemChanged(itemToDeselectIndex)
                    notifyItemChanged(selectedFilterPosition)

                    onFilterSelections.accept(it)

                }
                ?.let {
                    compositeDisposable.add(it)
                }
        }
    }

    fun dispose() {
        compositeDisposable.dispose()
    }

}