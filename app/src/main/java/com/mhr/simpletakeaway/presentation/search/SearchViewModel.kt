package com.mhr.simpletakeaway.presentation.search

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.BehaviorRelay
import com.mhr.simpletakeaway.domain.model.mapToPresentationModel
import com.mhr.simpletakeaway.domain.usecase.SearchUseCase
import com.mhr.simpletakeaway.presentation.model.RestaurantItem
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchViewModel @Inject constructor(private val searchUseCase: SearchUseCase) : ViewModel() {

    private val searchResults: BehaviorRelay<List<RestaurantItem>> = BehaviorRelay.create()
    private val errors: BehaviorRelay<String> = BehaviorRelay.create()

    private val compositeDisposable = CompositeDisposable()

    private var latestQuery = ""

    fun searchResults(): Observable<List<RestaurantItem>> = searchResults.hide()
    fun errors(): Observable<String> = errors.hide()

    fun searchBy(query: String) {

        if (query == latestQuery)
            return

        latestQuery = query
        searchUseCase.queryBy(query)
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    sortResults(it.mapToPresentationModel())
                },
                {
                    errors.accept(it.message ?: "")
                }
            )
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun sortResults(results: List<RestaurantItem>) {

        var comparator: Comparator<RestaurantItem> = Comparator { a, b ->
            when {
                b.isFavorite == a.isFavorite -> 0
                b.isFavorite -> 1
                else -> -1
            }
        }

        comparator = comparator.thenBy {
            it.status.ordinal
        }

        val sortedResults = results.sortedWith(comparator)
        searchResults.accept(sortedResults)

    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

}