package com.mhr.simpletakeaway.presentation.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.mhr.simpletakeaway.R

enum class SortFilter(@DrawableRes val icon: Int, @StringRes val title: Int) {
    BestMatch(
        R.drawable.ic_best_match_24dp,
        R.string.best_match
    ),
    Newest(
        R.drawable.ic_newest_24dp,
        R.string.newest
    ),
    Rating(
        R.drawable.ic_rating_24dp,
        R.string.rating_average
    ),
    Distance(
        R.drawable.ic_distance_24dp,
        R.string.distance
    ),
    Popularity(
        R.drawable.ic_popularity_24dp,
        R.string.popularity
    ),
    AveragePrice(
        R.drawable.ic_average_price_24dp,
        R.string.average_product_price
    ),
    DeliveryCost(
        R.drawable.ic_delivery_24dp,
        R.string.delivery_costs
    ),
    MinimumCost(
        R.drawable.ic_min_cost_24dp,
        R.string.minimum_cost
    )
}