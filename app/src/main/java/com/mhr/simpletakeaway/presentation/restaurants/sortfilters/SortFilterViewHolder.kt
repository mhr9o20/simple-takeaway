package com.mhr.simpletakeaway.presentation.restaurants.sortfilters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.jakewharton.rxbinding3.view.clicks
import com.mhr.simpletakeaway.presentation.model.SortFilter
import io.reactivex.Observable
import kotlinx.android.synthetic.main.view_sort_filter_item.view.*

class SortFilterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bind(item: SortFilter, isSelected: Boolean): Observable<SortFilter>? {

        itemView.sortFilterItemIconImageView?.setImageResource(item.icon)

        itemView.sortFilterItemRadioButton?.isChecked = isSelected
        itemView.sortFilterItemRadioButton?.setText(item.title)

        return itemView.sortFilterItemRadioButton?.clicks()?.map { item }

    }

}