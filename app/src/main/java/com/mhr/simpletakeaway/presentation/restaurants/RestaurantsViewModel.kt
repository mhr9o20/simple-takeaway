package com.mhr.simpletakeaway.presentation.restaurants

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.BehaviorRelay
import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.model.mapToPresentationModel
import com.mhr.simpletakeaway.domain.usecase.RestaurantsUseCase
import com.mhr.simpletakeaway.presentation.model.RestaurantItem
import com.mhr.simpletakeaway.presentation.model.SortFilter
import com.mhr.simpletakeaway.presentation.model.mapToDomainModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RestaurantsViewModel @Inject constructor(private val restaurantsUseCase: RestaurantsUseCase) : ViewModel() {

    private val restaurants: BehaviorRelay<List<RestaurantItem>> = BehaviorRelay.create()
    private val errors: BehaviorRelay<String> = BehaviorRelay.create()
    private val latestFilters: ArrayList<SortFilter> = arrayListOf()

    var latestFilter: SortFilter = SortFilter.Rating
        get() {
            return if (latestFilters.isEmpty()) SortFilter.Rating
            else latestFilters[0]
        }
        set(value) {
            field = value
            latestFilters.clear()
            latestFilters.add(field)
        }

    private val compositeDisposable = CompositeDisposable()

    fun restaurants(): Observable<List<RestaurantItem>> = restaurants.hide()
    fun errors(): Observable<String> = errors.hide()

    //region presentation Api
    fun fetch() {
        if (latestFilters.isEmpty()) latestFilter = SortFilter.Rating
        restaurantsUseCase.getAll()
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    sortByFiltersAndNotify(latestFilters, it.first.mapToPresentationModel())
                    checkAndUpdateCacheIfNecessary(it)
                },
                {
                    errors.accept(it.message ?: "")
                }
            )
            .let {
                compositeDisposable.add(it)
            }
    }

    fun sortBy(filters: ArrayList<SortFilter>) {

        latestFilters.clear()
        latestFilters.addAll(filters)

        restaurants.value?.let { restaurantList ->
            sortByFiltersAndNotify(filters, restaurantList)
        }
    }

    fun favour(restaurantItem: RestaurantItem) {
        restaurantItem.isFavorite = true
        updateAndNotify(restaurantItem.mapToDomainModel())
    }

    fun unFavour(restaurantItem: RestaurantItem) {
        restaurantItem.isFavorite = false
        updateAndNotify(restaurantItem.mapToDomainModel())
    }
    //endregion
    private fun updateAndNotify(restaurant: Restaurant) {
        restaurantsUseCase.update(restaurant)
            .subscribeOn(Schedulers.io())
            .andThen<Pair<List<Restaurant>, Boolean>>(restaurantsUseCase.getAll())
            .subscribe(
                {
                    sortByFiltersAndNotify(latestFilters, it.first.mapToPresentationModel())
                },
                {
                    errors.accept(it.message ?: "")
                }
            )
            .let {
                compositeDisposable.add(it)
            }
    }

    private fun sortByFiltersAndNotify(filters: ArrayList<SortFilter>, restaurantList: List<RestaurantItem>) {

        var comparator: Comparator<RestaurantItem> = Comparator { a, b ->
            when {
                b.isFavorite == a.isFavorite -> 0
                b.isFavorite -> 1
                else -> -1
            }
        }

        comparator = comparator.thenBy {
            it.status.ordinal
        }

        restaurantList.forEach {
            it.applySortFilters(filters)
        }

        comparator = comparator.thenByDescending {
            it.sortingScore
        }



        val sorted = restaurantList.sortedWith(comparator)
        restaurants.accept(sorted)
    }

    private fun checkAndUpdateCacheIfNecessary(result: Pair<List<Restaurant>, Boolean>) {
        if (result.second.not()) {
            restaurantsUseCase.setAll(result.first)
                .subscribeOn(Schedulers.io())
                .subscribe()
                .let {
                    compositeDisposable.add(it)
                }
        }
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

}