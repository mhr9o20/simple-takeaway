package com.mhr.simpletakeaway.presentation.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.jakewharton.rxbinding3.widget.textChanges
import com.mhr.simpletakeaway.App
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.presentation.model.RestaurantItem
import com.mhr.simpletakeaway.presentation.restaurants.RestaurantAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.view_search.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchFragment : Fragment() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var viewModel: SearchViewModel
    private lateinit var adapter: RestaurantAdapter
    private val inputPressureDelay = 400L
    private val inputPressureDelayTimeUnit = TimeUnit.MILLISECONDS

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
            R.layout.view_search,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        compositeDisposable = CompositeDisposable()

        App.component
            .viewModelComponentFactory
            .create()
            .inject(this)

        viewModel = ViewModelProviders.of(this, factory)[SearchViewModel::class.java]

        initViewInteractions(savedInstanceState)
        initDataInteractions(savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        closeKeyboard()
        compositeDisposable.dispose()
        adapter.dispose()
    }

    private fun initViewInteractions(savedInstanceState: Bundle?) {

        adapter = RestaurantAdapter()

        searchCloseButton?.setOnClickListener {
            findNavController().navigateUp()
        }

        searchInputEditText
            ?.textChanges()
            ?.debounce(inputPressureDelay, inputPressureDelayTimeUnit)
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(
                {
                    if (it.isNotBlank()) {
                        viewModel.searchBy(it.toString())
                    } else {
                        handleEmptyInput()
                    }
                }, {}
            )
            ?.let {
                compositeDisposable.add(it)
            }

        searchRecyclerView?.adapter = adapter

        openKeyboard()

    }

    private fun initDataInteractions(savedInstanceState: Bundle?) {

        viewModel.searchResults()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                handleNewData(it, true)
            }
            .let {
                compositeDisposable.add(it)
            }

        viewModel.errors()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                handleError(it)
            }
            .let {
                compositeDisposable.add(it)
            }

    }

    private fun handleEmptyInput() {
        handleNewData(listOf(), false)
    }

    private fun handleNewData(data: List<RestaurantItem>, isSearchResult: Boolean) {
        if (data.isEmpty()) {
            if (isSearchResult)
                searchEmptyPlaceHolderTextView?.setText(R.string.search_empty_placeholder)
            else
                searchEmptyPlaceHolderTextView?.text = ""
            searchEmptyPlaceHolderGroup?.visibility = View.VISIBLE
        } else {
            searchEmptyPlaceHolderGroup?.visibility = View.GONE
        }
        adapter.update(data)
    }

    private fun handleError(message: String) {
        if (message.isBlank()) {
            Toast.makeText(context, R.string.general_error, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun openKeyboard() {
        searchInputEditText?.requestFocus()
        (context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.showSoftInput(searchInputEditText, InputMethodManager.SHOW_IMPLICIT)
    }

    private fun closeKeyboard() {
        (context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow(view?.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

}