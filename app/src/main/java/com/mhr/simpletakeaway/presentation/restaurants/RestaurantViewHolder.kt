package com.mhr.simpletakeaway.presentation.restaurants

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.jakewharton.rxbinding3.view.clicks
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.presentation.model.RestaurantItem
import io.reactivex.Observable
import kotlinx.android.synthetic.main.view_restaurant_item.view.*
import kotlin.math.abs

class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: RestaurantItem): Observable<RestaurantItem>? {

        itemView.restaurantItemNameTextView?.text = item.name

        if (item.isFavorite) {
            itemView.restaurantItemFavouriteButton?.setImageResource(R.drawable.ic_star_fill_32dp)
        } else {
            itemView.restaurantItemFavouriteButton?.setImageResource(R.drawable.ic_star_border_32dp)
        }

        itemView.restaurantItemStatusTextView?.setText(item.status.title)
        val statusDrawable = VectorDrawableCompat.create(
            itemView.context.resources,
            item.status.icon,
            itemView.context.theme
        )
        itemView.restaurantItemStatusTextView?.setCompoundDrawablesRelativeWithIntrinsicBounds(
            statusDrawable, null, null, null
        )

        if (item.filters.isNullOrEmpty()) {
            itemView.restaurantItemSortFilterTextView?.visibility = View.GONE
        } else {

            val filter = item.filters!![0]

            val filterText = itemView.context.getString(filter.title)
            val placeholder = itemView.context.getString(R.string.sort_filter_placeholder)
            //NOTE: as we're setting only one filter from UI, we can simply assume the filter value like this:
            itemView.restaurantItemSortFilterTextView?.text =
                String.format(placeholder, filterText, "${abs(item.sortingScore)}")
            //NOTE: by changing to multiple filters, we have to update both layout and binding logic
            val filterValueText = itemView.restaurantItemSortFilterTextView?.text
            filterValueText?.let {

            }
            val sortFilterDrawable = VectorDrawableCompat.create(
                itemView.context.resources,
                filter.icon,
                itemView.context.theme
            )
            itemView.restaurantItemSortFilterTextView?.setCompoundDrawablesRelativeWithIntrinsicBounds(
                sortFilterDrawable, null, null, null
            )
            itemView.restaurantItemSortFilterTextView?.visibility = View.VISIBLE
        }

        return itemView.restaurantItemFavouriteButton?.clicks()?.map { item }

    }

}