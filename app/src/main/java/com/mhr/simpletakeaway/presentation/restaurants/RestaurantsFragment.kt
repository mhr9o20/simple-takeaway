package com.mhr.simpletakeaway.presentation.restaurants

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mhr.simpletakeaway.App
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.presentation.model.SortFilter
import com.mhr.simpletakeaway.presentation.restaurants.sortfilters.SortFiltersModal
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.view_restaurants.*
import javax.inject.Inject

class RestaurantsFragment : Fragment() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var viewModel: RestaurantsViewModel
    private lateinit var adapter: RestaurantAdapter

    private val stateBundlePositionKey = "POSITION_KEY"
    private val latestFilterPrefKey = "LATEST_FILTER"
    private val defaultFilter = SortFilter.Rating

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
            R.layout.view_restaurants,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        compositeDisposable = CompositeDisposable()

        App.component
            .viewModelComponentFactory
            .create()
            .inject(this)

        viewModel = ViewModelProviders.of(this, factory)[RestaurantsViewModel::class.java]

        initViewInteractions(savedInstanceState)
        initDataInteractions(savedInstanceState)
    }

    override fun onStop() {
        super.onStop()
        preserveLatestPreferences()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
        adapter.dispose()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        (restaurantsRecyclerView?.layoutManager as? LinearLayoutManager)
            ?.let {
                outState.putInt(
                    stateBundlePositionKey,
                    it.findFirstCompletelyVisibleItemPosition()
                )
            }
        super.onSaveInstanceState(outState)
    }

    private fun initViewInteractions(savedInstanceState: Bundle?) {

        adapter = RestaurantAdapter()

        restaurantsBottomAppBar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.restaurants_search -> {

                    findNavController().navigate(R.id.action_restaurantsFragment_to_searchFragment)

                    true
                }
                else -> false
            }
        }

        restaurantsFilterFab?.setOnClickListener {
            val sortFilterModal = SortFiltersModal(
                context!!,
                viewModel.latestFilter
            )
            sortFilterModal.onFilterSelections()
                .subscribe {
                    viewModel.sortBy(arrayListOf(it))
                }
                .let {
                    compositeDisposable.add(it)
                }
            sortFilterModal.show()
        }

        adapter.onFavouriteClicks()
            .subscribe {
                if (it.isFavorite) viewModel.unFavour(it)
                else viewModel.favour(it)
            }
            .let {
                compositeDisposable.add(it)
            }

        restaurantsRecyclerView?.adapter = adapter

    }

    private fun initDataInteractions(savedInstanceState: Bundle?) {

        if (savedInstanceState == null) {
            initLatestPreferences()
            viewModel.fetch()
        }

        viewModel.restaurants()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                adapter.update(it)
            }
            .let {
                compositeDisposable.add(it)
            }

        viewModel.errors()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                handleError(it)
            }
            .let {
                compositeDisposable.add(it)
            }

        //retaining last position after the data is set
        savedInstanceState?.getInt(stateBundlePositionKey)?.let {
            restaurantsRecyclerView?.post {
                restaurantsRecyclerView?.scrollToPosition(it)
            }
        }

    }

    private fun initLatestPreferences() {
        val latestFilterOrdinal = activity
            ?.getPreferences(Context.MODE_PRIVATE)
            ?.getInt(latestFilterPrefKey, defaultFilter.ordinal)
            ?: defaultFilter.ordinal

        viewModel.latestFilter = SortFilter.values()[latestFilterOrdinal]

    }

    private fun preserveLatestPreferences() {
        activity?.getPreferences(Context.MODE_PRIVATE)
            ?.edit()
            ?.putInt(
                latestFilterPrefKey,
                viewModel.latestFilter.ordinal
            )
            ?.apply()
    }

    private fun handleError(message: String) {
        if (message.isBlank()) {
            Toast.makeText(context, R.string.general_error, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }

}