package com.mhr.simpletakeaway.data.datasource

import androidx.room.*
import com.mhr.simpletakeaway.domain.model.Restaurant
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface RestaurantCacheDataSource {

    @Query("SELECT * FROM restaurant")
    fun getAll(): Single<List<Restaurant>>

    @Query("SELECT * FROM restaurant WHERE name LIKE :name LIMIT 1")
    fun get(name: String): Maybe<Restaurant>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun set(restaurant: Restaurant): Completable

    @Update
    fun update(restaurant: Restaurant): Completable

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun setAll(restaurants: List<Restaurant>): Completable

}

interface RestaurantFileDataSource {
    fun getAll(): Single<List<Restaurant>>
}