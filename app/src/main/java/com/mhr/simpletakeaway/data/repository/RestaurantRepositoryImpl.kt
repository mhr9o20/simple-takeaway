package com.mhr.simpletakeaway.data.repository

import com.mhr.simpletakeaway.data.datasource.RestaurantCacheDataSource
import com.mhr.simpletakeaway.data.datasource.RestaurantFileDataSource
import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.repository.RestaurantRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class RestaurantRepositoryImpl(
    private val cacheDataSource: RestaurantCacheDataSource,
    private val fileDataSource: RestaurantFileDataSource
) : RestaurantRepository {

    override fun getAll(): Single<Pair<List<Restaurant>, Boolean>> {

        return cacheDataSource
            .getAll()
            .flatMap { cachedList ->
                if (cachedList.isEmpty()) {
                    return@flatMap fileDataSource.getAll().map { Pair(it, false) }
                }
                return@flatMap Single.just(Pair(cachedList, true))
            }
    }

    override fun getBy(name: String): Maybe<Restaurant> {
        return cacheDataSource.get(name)
    }

    override fun set(restaurant: Restaurant): Completable {
        return cacheDataSource.set(restaurant)
    }

    override fun setAll(restaurants: List<Restaurant>): Completable {
        return cacheDataSource.setAll(restaurants)
    }

    override fun update(restaurant: Restaurant): Completable {
        return cacheDataSource.update(restaurant)
    }


}