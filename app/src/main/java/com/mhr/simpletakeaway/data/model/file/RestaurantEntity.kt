package com.mhr.simpletakeaway.data.model.file

import com.google.gson.annotations.SerializedName
import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.model.SortingValues
import com.mhr.simpletakeaway.domain.model.Status


data class RestaurantsEntity(
    @SerializedName("restaurants") val restaurants: List<RestaurantEntity>? = null
)

data class RestaurantEntity(
    @SerializedName("name") val name: String? = null,
    @SerializedName("status") val status: String? = null,
    @SerializedName("sortingValues") val sortingValues: SortingValuesEntity? = null
)

data class SortingValuesEntity(
    @SerializedName("bestMatch") val bestMatch: Double? = null,
    @SerializedName("newest") val newest: Double? = null,
    @SerializedName("ratingAverage") val ratingAverage: Double? = null,
    @SerializedName("distance") val distance: Double? = null,
    @SerializedName("popularity") val popularity: Double? = null,
    @SerializedName("averageProductPrice") val averageProductPrice: Double? = null,
    @SerializedName("deliveryCosts") val deliveryCosts: Double? = null,
    @SerializedName("minCost") val minCost: Double? = null
)

fun SortingValuesEntity.mapToDomainModel(): SortingValues = SortingValues(
    bestMatch = bestMatch ?: 0.0,
    newest = newest ?: 0.0,
    ratingAverage = ratingAverage ?: 0.0,
    distance = distance ?: 0.0,
    popularity = popularity ?: 0.0,
    averageProductPrice = averageProductPrice ?: 0.0,
    deliveryCosts = deliveryCosts ?: 0.0,
    minCost = minCost ?: 0.0
)

fun RestaurantEntity.mapToDomainModel(): Restaurant = Restaurant(
    name = name ?: "",
    status = status ?: Status.Closed,
    sortingValues = sortingValues?.mapToDomainModel() ?: SortingValues()
)

fun RestaurantsEntity.mapToDomainModel(): List<Restaurant> = restaurants?.map { it.mapToDomainModel() } ?: listOf()