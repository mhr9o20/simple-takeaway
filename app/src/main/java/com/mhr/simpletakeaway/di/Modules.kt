package com.mhr.simpletakeaway.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.mhr.simpletakeaway.data.repository.RestaurantRepositoryImpl
import com.mhr.simpletakeaway.datasource.cache.db.AppDatabase
import com.mhr.simpletakeaway.datasource.file.DataStreamer
import com.mhr.simpletakeaway.datasource.file.RestaurantFileDataSourceImpl
import com.mhr.simpletakeaway.domain.repository.RestaurantRepository
import com.mhr.simpletakeaway.domain.usecase.RestaurantsUseCase
import com.mhr.simpletakeaway.domain.usecase.SearchUseCase
import com.mhr.simpletakeaway.presentation.restaurants.RestaurantsViewModel
import com.mhr.simpletakeaway.presentation.search.SearchViewModel
import dagger.*
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton
import kotlin.reflect.KClass

@Module(subcomponents = [ViewModelComponent::class])
class AppModule {

    @Provides
    @Singleton
    fun restaurantRepository(context: Context): RestaurantRepository {

        val fileDataSource = RestaurantFileDataSourceImpl(
            DataStreamer(context.resources)
        )

        val db = Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            AppDatabase.DB_NAME
        ).build()

        val cacheDataSource = db.restaurantDao()

        return RestaurantRepositoryImpl(
            cacheDataSource,
            fileDataSource
        )

    }

    @Provides
    @Singleton
    fun restaurantUseCase(restaurantRepository: RestaurantRepository): RestaurantsUseCase {
        return RestaurantsUseCase(restaurantRepository)
    }

    @Provides
    @Singleton
    fun searchUseCase(restaurantRepository: RestaurantRepository): SearchUseCase {
        return SearchUseCase(restaurantRepository)
    }


}

class ViewModelFactory @Inject constructor(private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = viewModels[modelClass]?.get() as T
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(RestaurantsViewModel::class)
    internal abstract fun restaurantsViewModel(viewModel: RestaurantsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    internal abstract fun searchViewModel(viewModel: SearchViewModel): ViewModel

}