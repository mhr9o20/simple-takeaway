package com.mhr.simpletakeaway.datasource.cache.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.mhr.simpletakeaway.domain.model.SortingValues

class SortingValuesConverter {

    @TypeConverter
    fun fromJsonString(json: String?): SortingValues? {
        return json?.let {
            Gson().fromJson(it, SortingValues::class.java)
        }
    }

    @TypeConverter
    fun toJsonString(sortingValues: SortingValues?): String? {
        return sortingValues?.let {
            Gson().toJson(it)
        }
    }

}