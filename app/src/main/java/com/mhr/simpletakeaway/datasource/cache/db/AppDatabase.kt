package com.mhr.simpletakeaway.datasource.cache.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mhr.simpletakeaway.data.datasource.RestaurantCacheDataSource
import com.mhr.simpletakeaway.domain.model.Restaurant

@Database(entities = [Restaurant::class], version = 1)
@TypeConverters(SortingValuesConverter::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun restaurantDao(): RestaurantCacheDataSource

    companion object {
        const val DB_NAME = "my-database"
    }

}