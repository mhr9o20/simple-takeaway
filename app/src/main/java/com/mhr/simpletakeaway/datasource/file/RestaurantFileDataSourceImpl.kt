package com.mhr.simpletakeaway.datasource.file

import com.google.gson.Gson
import com.mhr.simpletakeaway.R
import com.mhr.simpletakeaway.data.datasource.RestaurantFileDataSource
import com.mhr.simpletakeaway.data.model.file.RestaurantsEntity
import com.mhr.simpletakeaway.data.model.file.mapToDomainModel
import com.mhr.simpletakeaway.domain.model.Restaurant
import io.reactivex.Single

class RestaurantFileDataSourceImpl(private val streamer: DataStreamer) : RestaurantFileDataSource {

    override fun getAll(): Single<List<Restaurant>> {
        return Single.create { emitter ->
            try {
                val json = streamer.streamJsonFile(R.raw.source)

                if (json.isNullOrBlank().not()) {

                    Gson().fromJson<RestaurantsEntity>(json, RestaurantsEntity::class.java)?.let {
                        emitter.onSuccess(it.mapToDomainModel())
                    } ?: run {
                        emitter.onError(Throwable())
                    }

                } else {
                    emitter.onError(Throwable())
                }

            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
    }

}