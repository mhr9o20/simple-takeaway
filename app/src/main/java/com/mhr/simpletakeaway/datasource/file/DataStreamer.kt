package com.mhr.simpletakeaway.datasource.file

import android.content.res.Resources
import androidx.annotation.RawRes

class DataStreamer(private val resources: Resources) {

    fun streamJsonFile(@RawRes resourceId: Int): String? {
        return resources
            .openRawResource(resourceId)
            .bufferedReader().use {
                it.readText()
            }
    }

}