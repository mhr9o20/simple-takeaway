package com.mhr.simpletakeaway.datasource.cache.db

import com.mhr.simpletakeaway.domain.model.SortingValues
import org.junit.Assert.*
import org.junit.Test

class SortingValuesConverterTest {

    private val sortingValuesJson = "{" +
            "\"bestMatch\":7.0," +
            "\"newest\":250.0," +
            "\"ratingAverage\":4.0," +
            "\"distance\":1396.0," +
            "\"popularity\":6.0," +
            "\"averageProductPrice\":912.0," +
            "\"deliveryCosts\":0.0," +
            "\"minCost\":1000.0" +
            "}"

    private val sortingValues = SortingValues(
        bestMatch = 7.0,
        newest = 250.0,
        ratingAverage = 4.0,
        distance = 1396.0,
        popularity = 6.0,
        averageProductPrice = 912.0,
        deliveryCosts = 0.0,
        minCost = 1000.0
    )

    private val someOtherSortingValues = SortingValues(
        bestMatch = 7.0,
        newest = 250.0,
        ratingAverage = 4.0,
        distance = 1396.0,
        popularity = 6.0,
        averageProductPrice = 912.0,
        deliveryCosts = 500.0,
        minCost = 1000.0
    )

    private val typeConverter = SortingValuesConverter()

    @Test
    fun testConversionFromJson() {
        val convertedModel = typeConverter.fromJsonString(sortingValuesJson)
        assertNotNull(convertedModel)
        assertEquals(sortingValues, convertedModel)
        assertNotEquals(someOtherSortingValues, convertedModel)
    }

    @Test
    fun testConversionToJson() {
        val convertedJson = typeConverter.toJsonString(sortingValues)
        assertNotNull(convertedJson)
        assertEquals(convertedJson, sortingValuesJson)
    }

}