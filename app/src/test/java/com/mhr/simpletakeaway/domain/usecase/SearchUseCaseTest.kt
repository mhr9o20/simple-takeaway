package com.mhr.simpletakeaway.domain.usecase

import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.repository.RestaurantRepository
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class SearchUseCaseTest {

    @Mock
    private lateinit var repository: RestaurantRepository
    private lateinit var searchUseCase: SearchUseCase
    private val compositeDisposable = CompositeDisposable()

    private val restaurant1 = Restaurant(name = "aAaB")
    private val restaurant2 = Restaurant(name = "AAA")
    private val restaurant3 = Restaurant(name = "Bb")
    private val restaurant4 = Restaurant(name = "aA B")
    private val restaurant5 = Restaurant(name = "a a aB")

    private val restaurants = listOf(
        restaurant1,
        restaurant2,
        restaurant3,
        restaurant4,
        restaurant5)

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        `when`(repository.getAll()).thenReturn(Single.just(Pair(restaurants, true)))

        searchUseCase = SearchUseCase(repository)
    }

    @Test
    fun testQueries() {
        val queryObserver1 = searchUseCase.queryBy("aa").test()
        val queryObserver2 = searchUseCase.queryBy("B").test()
        val queryObserver3 = searchUseCase.queryBy("Ab").test()
        val queryObserver4 = searchUseCase.queryBy("AaAb").test()
        val queryObserver5 = searchUseCase.queryBy("aa a").test()
        val queryObserver6 = searchUseCase.queryBy("b B").test()
        val queryObserver7 = searchUseCase.queryBy("").test()

        compositeDisposable.addAll(
            queryObserver1,
            queryObserver2,
            queryObserver3,
            queryObserver4,
            queryObserver5,
            queryObserver6,
            queryObserver7)

        queryObserver1.assertComplete()
        queryObserver1.assertNoErrors()
        queryObserver1.assertValue {
            it.size == 4 &&
                    it.contains(restaurant1) &&
                    it.contains(restaurant2) &&
                    it.contains(restaurant4) &&
                    it.contains(restaurant5)
        }

        queryObserver2.assertComplete()
        queryObserver2.assertNoErrors()
        queryObserver2.assertValue {
            it.size == 4 &&
                    it.contains(restaurant1) &&
                    it.contains(restaurant3) &&
                    it.contains(restaurant4) &&
                    it.contains(restaurant5)
        }

        queryObserver3.assertComplete()
        queryObserver3.assertNoErrors()
        queryObserver3.assertValue {
            it.size == 3 &&
                    it.contains(restaurant1) &&
                    it.contains(restaurant4) &&
                    it.contains(restaurant5)
        }

        queryObserver4.assertComplete()
        queryObserver4.assertNoErrors()
        queryObserver4.assertValue {
            it.size == 2 &&
                    it.contains(restaurant1) &&
                    it.contains(restaurant5)
        }

        queryObserver5.assertComplete()
        queryObserver5.assertNoErrors()
        queryObserver5.assertValue {
            it.size == 3 &&
                    it.contains(restaurant1) &&
                    it.contains(restaurant2) &&
                    it.contains(restaurant5)
        }

        queryObserver6.assertComplete()
        queryObserver6.assertNoErrors()
        queryObserver6.assertValue {
            it.size == 1 && it.contains(restaurant3)
        }

        queryObserver7.assertComplete()
        queryObserver7.assertNoErrors()
        queryObserver7.assertValue {
            it.size == 5 &&
                    it.contains(restaurant1) &&
                    it.contains(restaurant2) &&
                    it.contains(restaurant3) &&
                    it.contains(restaurant4) &&
                    it.contains(restaurant5)
        }

    }

    @After
    fun tearDown() {
        compositeDisposable.dispose()
    }

}