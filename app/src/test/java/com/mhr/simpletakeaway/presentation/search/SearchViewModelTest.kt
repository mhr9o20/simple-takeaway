package com.mhr.simpletakeaway.presentation.search

import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.model.Status
import com.mhr.simpletakeaway.domain.usecase.SearchUseCase
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyString
import org.mockito.MockitoAnnotations

class SearchViewModelTest {

    @Mock
    private lateinit var searchUseCase: SearchUseCase

    private lateinit var searchViewModel: SearchViewModel

    private val compositeDisposable = CompositeDisposable()

    @Before
    fun setUp() {

        MockitoAnnotations.initMocks(this)

        RxJavaPlugins.setIoSchedulerHandler {
            Schedulers.trampoline()
        }

        val restaurant1 = Restaurant(name = "A", isFavorite = false, status = Status.Open)
        val restaurant2 = Restaurant(name = "Aa", isFavorite = true, status = Status.OrderAhead)
        val restaurant3 = Restaurant(name = "Aaa", isFavorite = true, status = Status.Closed)
        val restaurant4 = Restaurant(name = "A aa", isFavorite = false, status = Status.Open)
        val restaurant5 = Restaurant(name = "A a a", isFavorite = true, status = Status.Closed)

        `when`(searchUseCase.queryBy(anyString())).thenReturn(
            Single.just(
                listOf(
                    restaurant1, restaurant2, restaurant3, restaurant4, restaurant5
                )
            )
        )

        searchViewModel = SearchViewModel(searchUseCase)

    }

    @Test
    fun testSortSearchResults() {

        searchViewModel.searchBy("not empty input")

        val resultsObserver = searchViewModel.searchResults().test()
        val errorsObserver = searchViewModel.errors().test()
        compositeDisposable.addAll(resultsObserver, errorsObserver)

        errorsObserver.assertNoValues()
        errorsObserver.assertNoErrors()

        resultsObserver.assertNoErrors()
        resultsObserver.assertValue {
            it[0].isFavorite &&
                    it[it.size - 1].isFavorite.not() &&
                    it[0].status.ordinal <= it[1].status.ordinal &&
                    it[1].status.ordinal <= it[2].status.ordinal
        }

    }

    @After
    fun tearDown() {
        compositeDisposable.dispose()
    }
}