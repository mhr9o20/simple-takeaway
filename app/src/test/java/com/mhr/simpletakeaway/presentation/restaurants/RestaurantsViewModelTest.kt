package com.mhr.simpletakeaway.presentation.restaurants

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mhr.simpletakeaway.domain.model.Restaurant
import com.mhr.simpletakeaway.domain.usecase.RestaurantsUseCase
import com.mhr.simpletakeaway.presentation.model.RestaurantItem
import com.mhr.simpletakeaway.presentation.model.SortFilter
import com.mhr.simpletakeaway.presentation.model.Status
import com.mhr.simpletakeaway.presentation.model.mapToDomainModel
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import kotlin.random.Random

class RestaurantsViewModelTest {

    @Mock
    lateinit var restaurantsUseCase: RestaurantsUseCase

    lateinit var restaurantsViewModel: RestaurantsViewModel

    private val itemToFavour = RestaurantItem(
        name = "Lunchpakketdienst",
        status = Status.Open,
        isFavorite = false,
        bestMatchScore = 306.0,
        newestScore = 259.0,
        ratingAverage = 3.5,
        distance = 14201.0,
        popularityScore = 0.0,
        averageProductPrice = 4465.0,
        deliveryCosts = 500.0,
        minimumCost = 5000.0
    )

    private val compositeDisposable = CompositeDisposable()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        RxJavaPlugins.setIoSchedulerHandler {
            Schedulers.trampoline()
        }

        //region json
        val json = "{\n" +
                "\t\"restaurants\": [{\n" +
                "\t\t\"name\": \"Tanoshii Sushi\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"isFavorite\": true,\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 0.0,\n" +
                "\t\t\t\"newest\": 96.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 1190,\n" +
                "\t\t\t\"popularity\": 17.0,\n" +
                "\t\t\t\"averageProductPrice\": 1536,\n" +
                "\t\t\t\"deliveryCosts\": 200,\n" +
                "\t\t\t\"minCost\": 1000\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Tandoori Express\",\n" +
                "\t\t\"status\": \"closed\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 1.0,\n" +
                "\t\t\t\"newest\": 266.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 2308,\n" +
                "\t\t\t\"popularity\": 123.0,\n" +
                "\t\t\t\"averageProductPrice\": 1146,\n" +
                "\t\t\t\"deliveryCosts\": 150,\n" +
                "\t\t\t\"minCost\": 1300\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Royal Thai\",\n" +
                "\t\t\"status\": \"order ahead\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 2.0,\n" +
                "\t\t\t\"newest\": 133.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 2639,\n" +
                "\t\t\t\"popularity\": 44.0,\n" +
                "\t\t\t\"averageProductPrice\": 1492,\n" +
                "\t\t\t\"deliveryCosts\": 150,\n" +
                "\t\t\t\"minCost\": 2500\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Sushi One\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 3.0,\n" +
                "\t\t\t\"newest\": 238.0,\n" +
                "\t\t\t\"ratingAverage\": 4.0,\n" +
                "\t\t\t\"distance\": 1618,\n" +
                "\t\t\t\"popularity\": 23.0,\n" +
                "\t\t\t\"averageProductPrice\": 1285,\n" +
                "\t\t\t\"deliveryCosts\": 0,\n" +
                "\t\t\t\"minCost\": 1200\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Roti Shop\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 4.0,\n" +
                "\t\t\t\"newest\": 247.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 2308,\n" +
                "\t\t\t\"popularity\": 81.0,\n" +
                "\t\t\t\"averageProductPrice\": 915,\n" +
                "\t\t\t\"deliveryCosts\": 0,\n" +
                "\t\t\t\"minCost\": 2000\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Aarti 2\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 5.0,\n" +
                "\t\t\t\"newest\": 153.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 1605,\n" +
                "\t\t\t\"popularity\": 44.0,\n" +
                "\t\t\t\"averageProductPrice\": 922,\n" +
                "\t\t\t\"deliveryCosts\": 250,\n" +
                "\t\t\t\"minCost\": 500\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Pizza Heart\",\n" +
                "\t\t\"status\": \"order ahead\",\n" +
                "\t\t\"isFavorite\": true,\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 6.0,\n" +
                "\t\t\t\"newest\": 118.0,\n" +
                "\t\t\t\"ratingAverage\": 4.0,\n" +
                "\t\t\t\"distance\": 2453,\n" +
                "\t\t\t\"popularity\": 9.0,\n" +
                "\t\t\t\"averageProductPrice\": 1103,\n" +
                "\t\t\t\"deliveryCosts\": 150,\n" +
                "\t\t\t\"minCost\": 1500\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Mama Mia\",\n" +
                "\t\t\"status\": \"order ahead\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 7.0,\n" +
                "\t\t\t\"newest\": 250.0,\n" +
                "\t\t\t\"ratingAverage\": 4.0,\n" +
                "\t\t\t\"distance\": 1396,\n" +
                "\t\t\t\"popularity\": 6.0,\n" +
                "\t\t\t\"averageProductPrice\": 912,\n" +
                "\t\t\t\"deliveryCosts\": 0,\n" +
                "\t\t\t\"minCost\": 1000\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Feelfood\",\n" +
                "\t\t\"status\": \"order ahead\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 8.0,\n" +
                "\t\t\t\"newest\": 163.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 2732,\n" +
                "\t\t\t\"popularity\": 31.0,\n" +
                "\t\t\t\"averageProductPrice\": 902,\n" +
                "\t\t\t\"deliveryCosts\": 150,\n" +
                "\t\t\t\"minCost\": 1500\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Daily Sushi\",\n" +
                "\t\t\"status\": \"closed\",\n" +
                "\t\t\"isFavorite\": true,\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 9.0,\n" +
                "\t\t\t\"newest\": 221.0,\n" +
                "\t\t\t\"ratingAverage\": 4.0,\n" +
                "\t\t\t\"distance\": 1911,\n" +
                "\t\t\t\"popularity\": 6.0,\n" +
                "\t\t\t\"averageProductPrice\": 1327,\n" +
                "\t\t\t\"deliveryCosts\": 200,\n" +
                "\t\t\t\"minCost\": 1000\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Pamukkale\",\n" +
                "\t\t\"status\": \"closed\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 10.0,\n" +
                "\t\t\t\"newest\": 201.0,\n" +
                "\t\t\t\"ratingAverage\": 4.0,\n" +
                "\t\t\t\"distance\": 2353,\n" +
                "\t\t\t\"popularity\": 25.0,\n" +
                "\t\t\t\"averageProductPrice\": 968,\n" +
                "\t\t\t\"deliveryCosts\": 0,\n" +
                "\t\t\t\"minCost\": 2000\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Indian Kitchen\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 11.0,\n" +
                "\t\t\t\"newest\": 272.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 2308,\n" +
                "\t\t\t\"popularity\": 5.0,\n" +
                "\t\t\t\"averageProductPrice\": 1189,\n" +
                "\t\t\t\"deliveryCosts\": 150,\n" +
                "\t\t\t\"minCost\": 1300\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"CIRO 1939\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 12.0,\n" +
                "\t\t\t\"newest\": 231.0,\n" +
                "\t\t\t\"ratingAverage\": 4.5,\n" +
                "\t\t\t\"distance\": 3957,\n" +
                "\t\t\t\"popularity\": 79.0,\n" +
                "\t\t\t\"averageProductPrice\": 1762,\n" +
                "\t\t\t\"deliveryCosts\": 99,\n" +
                "\t\t\t\"minCost\": 1300\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Zenzai Sushi\",\n" +
                "\t\t\"status\": \"closed\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 13.0,\n" +
                "\t\t\t\"newest\": 155.0,\n" +
                "\t\t\t\"ratingAverage\": 4.0,\n" +
                "\t\t\t\"distance\": 2911,\n" +
                "\t\t\t\"popularity\": 36.0,\n" +
                "\t\t\t\"averageProductPrice\": 1579,\n" +
                "\t\t\t\"deliveryCosts\": 0,\n" +
                "\t\t\t\"minCost\": 2000\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Fes Patisserie\",\n" +
                "\t\t\"status\": \"order ahead\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 14.0,\n" +
                "\t\t\t\"newest\": 77.0,\n" +
                "\t\t\t\"ratingAverage\": 4.0,\n" +
                "\t\t\t\"distance\": 2302,\n" +
                "\t\t\t\"popularity\": 3.0,\n" +
                "\t\t\t\"averageProductPrice\": 1214,\n" +
                "\t\t\t\"deliveryCosts\": 150,\n" +
                "\t\t\t\"minCost\": 1250\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Yvonne's Vispaleis\",\n" +
                "\t\t\"status\": \"order ahead\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 15.0,\n" +
                "\t\t\t\"newest\": 150.0,\n" +
                "\t\t\t\"ratingAverage\": 5.0,\n" +
                "\t\t\t\"distance\": 2909,\n" +
                "\t\t\t\"popularity\": 3.0,\n" +
                "\t\t\t\"averageProductPrice\": 2557,\n" +
                "\t\t\t\"deliveryCosts\": 150,\n" +
                "\t\t\t\"minCost\": 1750\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"De Amsterdamsche Tram\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 304.0,\n" +
                "\t\t\t\"newest\": 131.0,\n" +
                "\t\t\t\"ratingAverage\": 0.0,\n" +
                "\t\t\t\"distance\": 2792,\n" +
                "\t\t\t\"popularity\": 0.0,\n" +
                "\t\t\t\"averageProductPrice\": 892,\n" +
                "\t\t\t\"deliveryCosts\": 0,\n" +
                "\t\t\t\"minCost\": 0\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Lale Restaurant & Snackbar\",\n" +
                "\t\t\"status\": \"order ahead\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 305.0,\n" +
                "\t\t\t\"newest\": 73.0,\n" +
                "\t\t\t\"ratingAverage\": 0.0,\n" +
                "\t\t\t\"distance\": 2880,\n" +
                "\t\t\t\"popularity\": 0.0,\n" +
                "\t\t\t\"averageProductPrice\": 838,\n" +
                "\t\t\t\"deliveryCosts\": 0,\n" +
                "\t\t\t\"minCost\": 0\n" +
                "\t\t}\n" +
                "\t}, {\n" +
                "\t\t\"name\": \"Lunchpakketdienst\",\n" +
                "\t\t\"status\": \"open\",\n" +
                "\t\t\"sortingValues\": {\n" +
                "\t\t\t\"bestMatch\": 306.0,\n" +
                "\t\t\t\"newest\": 259.0,\n" +
                "\t\t\t\"ratingAverage\": 3.5,\n" +
                "\t\t\t\"distance\": 14201,\n" +
                "\t\t\t\"popularity\": 0.0,\n" +
                "\t\t\t\"averageProductPrice\": 4465,\n" +
                "\t\t\t\"deliveryCosts\": 500,\n" +
                "\t\t\t\"minCost\": 5000\n" +
                "\t\t}\n" +
                "\t}]\n" +
                "}"
        //endregion

        val jsonObject = JSONObject(json)
        val restaurantsArray = jsonObject.getJSONArray("restaurants")

        val type = object : TypeToken<List<Restaurant>>() {}.type
        val restaurants = Gson().fromJson<List<Restaurant>>(restaurantsArray.toString(), type)
        val favoriteRestaurants = Gson().fromJson<List<Restaurant>>(restaurantsArray.toString(), type)
        favoriteRestaurants[favoriteRestaurants.indexOf(itemToFavour.mapToDomainModel())].isFavorite = true

        `when`(restaurantsUseCase.getAll())
            .thenReturn(
                Single.just(Pair(restaurants, true)),
                Single.just(Pair(favoriteRestaurants, true)),
                Single.just(Pair(restaurants, true))
            )

        restaurantsViewModel = RestaurantsViewModel(restaurantsUseCase)

        restaurantsViewModel.fetch()

    }

    @Test
    fun fetch() {

        val restaurantsObserver = restaurantsViewModel.restaurants().test()
        val errorsObserver = restaurantsViewModel.errors().test()
        compositeDisposable.addAll(restaurantsObserver, errorsObserver)

        errorsObserver.assertNoErrors()
        errorsObserver.assertNoValues()

        restaurantsObserver.assertNoErrors()
        restaurantsObserver.assertValue {
            it.isNotEmpty() &&
                    it[0].isFavorite &&
                    it[0].status == Status.Open &&
                    it[0].status.ordinal <= it[1].status.ordinal &&
                    it[1].status.ordinal <= it[2].status.ordinal &&
                    it[it.size - 1].isFavorite.not() &&
                    it[it.size - 1].status == Status.Closed
        }

    }

    @Test
    fun sortBy() {
        for (i in 0 until 10) {
            assertSortFilters()
        }
    }

    @Test
    fun favourAndUnFavour() {
        val favourObserver = restaurantsViewModel.restaurants()
            .skip(2)
            .test()
        val errorsObserver = restaurantsViewModel.errors().test()

        restaurantsViewModel.sortBy(arrayListOf(SortFilter.BestMatch))

        `when`(restaurantsUseCase.update(itemToFavour.mapToDomainModel())).thenReturn(
            Completable.complete()
        )

        restaurantsViewModel.favour(itemToFavour)

        errorsObserver.assertNoErrors()
        errorsObserver.assertNoValues()

        favourObserver.assertNoErrors()
        favourObserver.assertValueAt(0) {
            it.isNotEmpty() &&
                    it[0] == itemToFavour &&
                    it[0].bestMatchScore > it[1].bestMatchScore
        }

        restaurantsViewModel.unFavour(itemToFavour)

        errorsObserver.assertNoErrors()
        errorsObserver.assertNoValues()

        favourObserver.assertValueAt(1) {
            it.isNotEmpty() &&
                    it[0] != itemToFavour
        }


    }

    @After
    fun tearDown() {
        compositeDisposable.dispose()
    }

    private fun assertSortFilters() {
        val allFilters = mutableListOf(
            SortFilter.Rating,
            SortFilter.Popularity,
            SortFilter.Newest,
            SortFilter.MinimumCost,
            SortFilter.Distance,
            SortFilter.DeliveryCost,
            SortFilter.AveragePrice,
            SortFilter.BestMatch
        )

        val randomFilterCount = Random.nextInt(3, 8)
        val randomFilters: HashSet<SortFilter> = hashSetOf()

        for (i in 0 until randomFilterCount) {
            val randomIndex = Random.nextInt(0, allFilters.size)
            randomFilters.add(allFilters[randomIndex])
            allFilters.removeAt(randomIndex)
        }

        val restaurantsObserver = restaurantsViewModel.restaurants()
            .skip(1L)
            .test()
        val errorsObserver = restaurantsViewModel.errors().test()
        compositeDisposable.addAll(restaurantsObserver, errorsObserver)

        val filtersList = ArrayList<SortFilter>(randomFilters)

        restaurantsViewModel.sortBy(filtersList)

        errorsObserver.assertNoErrors()
        errorsObserver.assertNoValues()

        restaurantsObserver.assertNoErrors()
        restaurantsObserver.assertValue {
            it.isNotEmpty() &&
                    it[0].isFavorite &&
                    it[0].status == Status.Open &&
                    it[0].status.ordinal <= it[1].status.ordinal &&
                    it[1].status.ordinal <= it[2].status.ordinal &&
                    it[it.size - 1].isFavorite.not() &&
                    it[it.size - 1].status == Status.Closed
        }

        restaurantsObserver.assertValue {

            it[3].sortingScore > it[4].sortingScore
            it[4].sortingScore > it[5].sortingScore
            it[5].sortingScore > it[6].sortingScore

        }
    }

}