package com.mhr.simpletakeaway.presentation.model

import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import kotlin.random.Random

class RestaurantItemTest {

    private lateinit var sampleItem: RestaurantItem

    @Before
    fun setUp() {
        sampleItem = RestaurantItem(
            name = "My Luxury Restaurant",
            status = Status.Open, //always!
            isFavorite = true, //for sure!
            bestMatchScore = Random.nextDouble(100.0),
            newestScore = Random.nextDouble(200.0),
            ratingAverage = Random.nextDouble(5.0),
            distance = Random.nextDouble(2000.0),
            popularityScore = Random.nextDouble(300.0),
            averageProductPrice = Random.nextDouble(750.0, 2000.0),
            deliveryCosts = Random.nextDouble(1500.0),
            minimumCost = Random.nextDouble(250.0, 750.0)
        )
    }

    /*
     * Rewriting this kind of methods by writing a test function
     * has always helped me preventing human errors
     */
    @Test
    fun validateFilterScore() {

        val allFilters = mutableListOf(
            SortFilter.Rating,
            SortFilter.Popularity,
            SortFilter.Newest,
            SortFilter.MinimumCost,
            SortFilter.Distance,
            SortFilter.DeliveryCost,
            SortFilter.AveragePrice,
            SortFilter.BestMatch)

        val randomFilterCount = Random.nextInt(3, 8)
        val randomFilters: HashSet<SortFilter> = hashSetOf()

        for (i in 0 until randomFilterCount) {
            val randomIndex = Random.nextInt(0, allFilters.size)
            randomFilters.add(allFilters[randomIndex])
            allFilters.removeAt(randomIndex)
        }

        var expectedScore = 0.0

        for (filter in randomFilters) {
            when (filter) {
                SortFilter.AveragePrice -> expectedScore -= sampleItem.averageProductPrice
                SortFilter.DeliveryCost -> expectedScore -= sampleItem.deliveryCosts
                SortFilter.Distance -> expectedScore -= sampleItem.distance
                SortFilter.BestMatch -> expectedScore += sampleItem.bestMatchScore
                SortFilter.MinimumCost -> expectedScore -= sampleItem.minimumCost
                SortFilter.Popularity -> expectedScore += sampleItem.popularityScore
                SortFilter.Rating -> expectedScore += sampleItem.ratingAverage
                SortFilter.Newest -> expectedScore += sampleItem.newestScore
            }
        }

        val appliedFilters = ArrayList(randomFilters)

        sampleItem.applySortFilters(appliedFilters)

        assertEquals(expectedScore.compareTo(sampleItem.sortingScore), 0)
        assertNotNull(sampleItem.filters)
        assertEquals(appliedFilters, sampleItem.filters)

    }

    @After
    fun tearDown() {
    }
}