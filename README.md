# Simple Takeaway
## Overview

***Simple Takeaway*** is a simple application, which shows a list of restaurants by their names, opening status and a selected sorting filter.
It also gives users the ability to search Restaurants by their names.

## Technical Overview
The app is developed upon MVVM architecture. It has two Data-Sources:

 - Local Json File
 - Cache / Local Database
 
The json file acts as an initialiser, afterwards every read/write is on the database. If user clears the app data, the modified data will be wiped [which are the Favourite Restaurants].

ViewModels, Repositories, the logic of Data-Srouce Implementations and Use-Cases [if any] are all unit-tested.

> Note that the Room-related tests [RestaurantRepository Implementation & AppDatabase] are located in "androidTest" folder.

DataFlow is provided using Dagger2 and finally at the domain-layer [Use-Cases] is injected into ViewModels.

> Database entity is defined on domain-layer, as it carries domain-layer logics.
> Becasue "isFavourite" is not defined on entity-layer concerning File data-source as our root.

The expected basic "UI behaviour" is tested by Espresso. You can run `AppUiFlowTest` inside `androidTest` package to check it.  

## How to Run
You can clone the project through `HTTPS` using `git clone https://mhr9o20@bitbucket.org/mhr9o20/simple-takeaway.git`
or via `SSH` using `git clone git@bitbucket.org:mhr9o20/simple-takeaway.git`

## Design
App supports Material DayNight theme, so you can enjoy the dark one on Android 10 devices.
The theme pallets are:

 - [Day Theme](https://material.io/resources/color/#!/?view.left=1&view.right=1&primary.color=FFFFFF&secondary.color=ff7b2d&primary.text.color=222222&secondary.text.color=222222)
 - [Night Theme](https://material.io/resources/color/#!/?view.left=0&view.right=1&primary.color=263238&secondary.color=ff7b2d&secondary.text.color=222222&primary.text.color=ffffff)

Icons are from AndroidStudio built-in Material Icon pack.
The illustration icons are made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](http://www.flaticon.com)
